#
# 
#
use strict;
use libinstall;
use installvars;
use EmulabConstants;
use OSImage;
use OSinfo;

my $UTAHURL       = "http://www.emulab.net/downloads";
my $DESCRIPTORS   = "$TOP_SRCDIR/install/descriptors-v4.xml";
my $GENDEV        = "$TOP_SRCDIR/install/descriptors-gendev.xml";
my @MBRS          = ("emulab-mbr.dd", "emulab-mbr2.dd", "emulab-mbr3.dd");

my $LINUXSTDIMAGE = "UBUNTU14-64-STD";
my $FBSDSTDIMAGE  = "FBSD103-64-STD";
my $DEFAULTIMAGE  = $LINUXSTDIMAGE;

#
# We now use image import.
#
my %STDIMAGES     = (
    "UBUNTU14-64-STD"  => "https://www.emulab.net/image_metadata.php?uuid=0a29c738-32b3-11e4-b30a-001143e453fe",
    "CENTOS7-64-STD"   => "https://www.emulab.net/image_metadata.php?uuid=6fa68fd6-9163-11e6-ac8c-90e2ba22fee4",
    "FBSD103-64-STD"   => "https://www.emulab.net/image_metadata.php?uuid=f3866e90-2464-11e6-bdf0-d1afad303f71",
    "XEN46-64-STD"     => "https://www.emulab.net/image_metadata.php?uuid=61799d52-4dff-11e6-ac8a-90e2ba22fee4",
);

my %NEWIMAGES     = (
    "UBUNTU16-64-STD"  => "https://www.emulab.net/image_metadata.php?uuid=7683ca8e-5e37-11e6-ac8a-90e2ba22fee4"
);

#
# Old style mappings, but probably still in use someplace.
#
my %STDMAPPINGS = (
    "RHL-STD"   => $LINUXSTDIMAGE,
    "FBSD-STD"  => $FBSDSTDIMAGE,
);

sub Install($$$)
{
    my ($server, $isupdate, $impotent) = @_;

    # Replace if this script does an update for ip/domain.
    return 0
	if ($isupdate);

    Phase "IDs", "Setting up Images and OSs", sub {
	if ($PROTOGENI_GENIRACK) {
	    require NodeType;
	    
	    #
	    # Load up the nodetype. It will be the same on every machine,
	    # and if we do it before loading the descriptors, then we
	    # do not have to go back and change the type mappings.
	    #
	    my $NODETYPE     = "dl360";
	    my $CREATETYPE   = "$PREFIX/bin/editnodetype";
	    my $NODETYPEXML  = "$TOP_SRCDIR/install/genirack/nodetype.xml";
	    
	    Phase "nodetype", "Creating Protogeni nodetype", sub {
		my $nodetype = NodeType->Lookup($NODETYPE);
		PhaseSkip("already created")
		    if (defined($nodetype));
		ExecQuietFatal("$SUDO -u $PROTOUSER ".
			       "         $WAP $CREATETYPE $NODETYPEXML");
	    };
	}
	
	#
	# Load up the initial descriptors. 
	# Load up the images from boss.
	#
	Phase "descriptors", "Loading the Image and OS IDs", sub {
	    ExecQuietFatal("cd $TOP_OBJDIR/install; ".
			   "   $SUDO -u $PROTOUSER $WAP ".
			   "   perl load-descriptors -a $DESCRIPTORS");
	};
	Phase "gendev", "Loading additional OS IDs", sub {
	    ExecQuietFatal("cd $TOP_OBJDIR/install; ".
			   "   $SUDO -u $PROTOUSER $WAP ".
			   "   perl load-descriptors $GENDEV");
	};
	foreach my $imagename (keys(%STDIMAGES)) {
	    my $url = $STDIMAGES{$imagename};
	    
	    Phase "$imagename", "Fetching $imagename. Patience!", sub {
		my $image = OSImage->Lookup(TBOPSPID(), $imagename);
		PhaseSkip("already fetched")
		    if (defined($image));

		ExecQuietFatal("$SUDO -u $PROTOUSER ".
			       "         $WAP $IMAGEIMPORT -g '$url'");
	    };
	}
	foreach my $mbr (@MBRS) {
	    my $localfile = "$PREFIX/images/$mbr";
	    my $url       = "$UTAHURL/$mbr";
	    
	    Phase "$mbr", "Fetching $mbr.", sub {
		DoneIfExists($localfile);
		FetchFileFatal($url, $localfile);
	    };
	}
	foreach my $osname (keys(%STDMAPPINGS)) {
	    my $nextosname = $STDMAPPINGS{$osname};

	    Phase "$osname", "Mapping $osname to $nextosname", sub {
		my $osinfo = OSinfo->LookupByName($osname);
		PhaseFail("Could not look up $osname osid")
		    if (!defined($osinfo));
		PhaseSkip("already set")
		    if (defined($osinfo->nextosid()));
	    
		my $nextosinfo = OSinfo->LookupByName($nextosname);
		PhaseFail("Could not look up $nextosname osid")
		    if (!defined($nextosinfo));
		$osinfo->SetNextOS($nextosinfo) == 0
		    or PhaseFail("Could not set the nextosid");
	    };
	}
	
	#
	# The sitevar is how the reload daemon knows what to reload nodes
	# with, by default.
	#
	Phase "sitevar", "Setting sitevar $DEFAULTIMAGESITEVAR", sub {
	    ExecQuietFatal("$PREFIX/sbin/setsitevar ".
			   "'$DEFAULTIMAGESITEVAR' '$DEFAULTIMAGE'");
	};

	if ($PROTOGENI_GENIRACK) {
	    #
	    # Load up second half of the nodetype, which defines the osids
	    # and images it needs, which has to be done after the above.
	    #
	    my $NODETYPE     = "dl360";
	    my $CREATETYPE   = "$PREFIX/bin/editnodetype";
	    my $NODETYPEXML  = "$TOP_SRCDIR/install/genirack/nodetypeB.xml";
	    
	    Phase "nodetype", "Creating Protogeni nodetype", sub {
		ExecQuietFatal("$SUDO -u $PROTOUSER ".
			       "         $WAP $CREATETYPE $NODETYPEXML");
	    };
	}
	
	PhaseSucceed("done")
    };
    return 0;
}

# Local Variables:
# mode:perl
# End:
