<?php
#
# Copyright (c) 2000-2017 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
chdir("..");
include_once("webtask.php");
chdir("apt");
include_once("ajax-routines.ajax");

# We set this in CheckPageArgs
$target_user = null;

#
# Need to check the permission, since we allow admins to mess with
# other accounts.
#
function CheckPageArgs()
{
    global $this_user, $target_user;
    global $ajax_args;
    global $TB_USERINFO_READINFO;

    if (!isset($ajax_args["uid"])) {
	SPITAJAX_ERROR(-1, "Missing target uid");
	return -1;
    }
    $uid = $ajax_args["uid"];
    
    if (!TBvalid_uid($uid)) {
	SPITAJAX_ERROR(-1, "Invalid target uid");
        return -1;
    }
    $target_user = User::Lookup($uid);
    if (!$target_user) {
        sleep(2);
	SPITAJAX_ERROR(-1, "Unknown target uid");
        return -1;
    }
    if ($uid == $this_user->uid())
        return 0;
    
    if (!ISADMIN() && !ISFOREIGN_ADMIN() &&
        !$target_user->AccessCheck($this_user, $TB_USERINFO_READINFO)) {
	SPITAJAX_ERROR(-1, "Not enough permission");
	return -1;
    }
    return 0;
}

#
# List images at a cluster (for a user).
#
function Do_ListImages()
{
    global $this_user, $target_user;
    global $ajax_args;
    global $TB_PROJECT_CREATEEXPT, $suexec_output;

    if (CheckPageArgs()) {
        return;
    }
    if (!isset($ajax_args["cluster"])) {
	SPITAJAX_ERROR(-1, "Missing cluster");
	return;
    }
    if (!preg_match("/^[-\w]+$/", $ajax_args["cluster"])) {
        SPITAJAX_ERROR(-1, "Invalid cluster name");
        return;
    }
    $aggregate = Aggregate::LookupByNickname($ajax_args["cluster"]);
    if (!$aggregate) {
        SPITAJAX_ERROR(-1, "No such cluster");
        return;
    }
    $uid = $target_user->uid();
    $urn = $aggregate->urn();
    $webtask = WebTask::CreateAnonymous();
    $webtask_id = $webtask->task_id();

    $retval = SUEXEC($uid, "nobody",
                     "webmanage_images -t $webtask_id list -a '$urn'",
                     SUEXEC_ACTION_CONTINUE);

    if ($retval) {
        $webtask->Delete();
        SPITAJAX_ERROR(-1, $suexec_output);
	return;
    }
    $webtask->Refresh();
    $images = $webtask->TaskValue("value");
    $webtask->Delete();
    SPITAJAX_RESPONSE($images);
}

#
# Delete image at a cluster (for a user).
#
function Do_DeleteImage()
{
    global $this_user, $target_user;
    global $ajax_args;
    global $suexec_output;
    $pdarg = "";

    if (CheckPageArgs()) {
        return;
    }
    if (!isset($ajax_args["urn"]) || $ajax_args["urn"] == "") {
	SPITAJAX_ERROR(-1, "Missing image urn");
	return;
    }
    $image_urn = escapeshellarg($ajax_args["urn"]);
    
    if (!isset($ajax_args["cluster"])) {
	SPITAJAX_ERROR(-1, "Missing cluster");
	return;
    }
    if (!preg_match("/^[-\w]+$/", $ajax_args["cluster"])) {
        SPITAJAX_ERROR(-1, "Invalid cluster name");
        return;
    }
    $aggregate = Aggregate::LookupByNickname($ajax_args["cluster"]);
    if (!$aggregate) {
        SPITAJAX_ERROR(-1, "No such cluster");
        return;
    }
    if (isset($ajax_args["profile-delete"]) &&
        $ajax_args["profile-delete"] != "") {
        if (!preg_match("/^[-\w]+$/", $ajax_args["profile-delete"])) {
            SPITAJAX_ERROR(-1, "Invalid profile uuid for deletion");
            return;
        }
        $pdarg = "-d " . escapeshellarg($ajax_args["profile-delete"]);
    }
    
    $uid = $target_user->uid();
    $aggurn = $aggregate->urn();
    $webtask = WebTask::CreateAnonymous();
    $webtask_id = $webtask->task_id();

    $retval = SUEXEC($uid, "nobody",
                     "webmanage_images -t $webtask_id ".
                     "  delete -a '$aggurn' $pdarg $image_urn",
                     SUEXEC_ACTION_CONTINUE);

    if ($retval) {
        $webtask->Delete();
        SPITAJAX_ERROR(-1, $suexec_output);
	return;
    }
    $webtask->Delete();
    SPITAJAX_RESPONSE(0);
}

#
# Classic images on the local cluster.
#
function Do_ClassicImageList()
{
    global $this_user, $target_user;
    global $ajax_args;
    global $TB_PROJECT_CREATEEXPT, $suexec_output;

    if (CheckPageArgs()) {
        return;
    }
    $images = ClassicImageList($target_user);
    SPITAJAX_RESPONSE($images);
}

# Local Variables:
# mode:php
# End:
?>
